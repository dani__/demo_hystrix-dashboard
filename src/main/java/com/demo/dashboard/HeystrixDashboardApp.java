package com.demo.dashboard;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.turbine.EnableTurbine;


@SpringBootApplication
@EnableHystrixDashboard
@EnableTurbine
public class HeystrixDashboardApp 
{
    public static void main( String[] args )
    {
    	new SpringApplicationBuilder(HeystrixDashboardApp.class).web(true).run(args);
    }
}
